//
//  HeroStats.swift
//  dota(decodable)
//
//  Created by amr on 3/13/18.
//  Copyright © 2018 amr. All rights reserved.
//

import Foundation
struct HeroStats:Decodable {
    let localized_name: String
    let primary_attr: String
    let attack_type: String
    let legs: Int
    let img: String
}
